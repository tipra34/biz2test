# Overview

This project solves the test problem provide by Biz2Credit.

## Problem Statement

We have some customer records in a text file (customers.txt) -- one customer per line, JSON
lines formatted. We want to invite any customer within 100km of Dublin for some food and
drinks on us. Write a program that will read the full list of customers and output the names and
user ids of matching customers (within 100km), sorted by User ID (ascending).

## Solution

The project has been divided into 2 parts:
1. Search algo:
    published as npm package on : https://www.npmjs.com/package/geonear
    which exposes required api to filter a list of objects with keyes
    {latitude, longitude} which are upto x km distance from given point.
    Note: readme provided in package as well as at end of the file

2. Test script:
    test.js which read input from data/customers file and uses geonear to solve the problem.
    The output is printed on the console as no output specs were given.

## Dependencies
  geonear--->npm package

## Run Code
npm run start


# Geonear Package
Find coordinates from list of coordinates that are near to x km from given location.
## Install
```bash
npm i -S geonear
```
## Usage
```
const geonear = require('geonear')

//find distance
let distance = geonear.findDistance(lat1, lon1, lat2, lon2)

//find x km near coordinates from point
const distanceFrom = {latitude:20, longitude:30}
const maxDistance = 100 //in km
const coordinates = [{id: 1, latitude:1, longitude:2},{id: 1, latitude:1, longitude:2}]
const filteredPoints = geonear.findXKmNearCoorsdinates(distanceFrom, coordinates, maxDistance)
```
