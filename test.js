const fs = require('fs')
const geonear = require('geonear')

const dublin = {latitude: 53.339428, longitude:-6.257664}

//read customer data
const rawData = fs.readFileSync('./data/customers', 'utf8').trim()
let customers = JSON.parse(`[${rawData.split('\n').join(',')}]`)

//find nearby customers, sort them and extract user_id and name
let nearCustomers = geonear.findXKmNearCoorsdinates(dublin, customers, 100)
                    .sort((a,b)=>{
                      return a.user_id - b.user_id
                    }).map((customer)=>{
                      return {user_id: customer.user_id, name:customer.name}
                    })

//output
console.log(JSON.stringify(nearCustomers, null, 2))
